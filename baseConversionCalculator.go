package conversion_fee

import (
	"context"
	"fmt"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// baseConversionCalculator реализует сервис конвертации значений
type baseConversionCalculator struct {
	roundingService   rounding.Service
	currencyConverter currencyConverter
}

// newBaseConversionCalculator реализует конструктор конвертера
func newBaseConversionCalculator() conversionCalculator {
	return &baseConversionCalculator{
		roundingService:   rounding.New(),
		currencyConverter: newBaseCurrencyConverter(),
	}
}

// CalculateConversion вычисляет процент конверсионных издержек
// для переданных параметров конвертации. Возвращает округленное
// значение конверсионных издержек и суммы с конверсией
func (b baseConversionCalculator) CalculateConversion(
	ctx context.Context,
	sourceCurrency CurrencyEntity,
	targetCurrency CurrencyEntity,
	configuration ConversionConfiguration,
	price float64,
	isNeedLog bool,
) (float64, float64, string, string) {
	if h.IsCtxDone(ctx) {
		return 0, 0, ``, ``
	}

	log := ``
	sumLog := ``

	if price <= 0 {
		return 0, 0, `0`, `0`
	}

	percentConversionFee := price / 100 * float64(configuration.Percent)
	if isNeedLog {
		log += fmt.Sprintf(`Percent amount [ %v / 100 * %v ]`, price, configuration.Percent)
	}

	if configuration.IsMinValueLimited && percentConversionFee < configuration.MinValue {
		if isNeedLog {
			log = fmt.Sprintf(`MIN[%v, min :: %v]`, log, configuration.MinValue)
		}

		percentConversionFee = configuration.MinValue
	}

	if configuration.IsMaxValueLimited && percentConversionFee > configuration.MaxValue {
		if isNeedLog {
			log = fmt.Sprintf(`MAX[%v, max :: %v]`, log, configuration.MaxValue)
		}

		percentConversionFee = configuration.MaxValue
	}

	totalConversionFee := percentConversionFee + configuration.FixAmount
	if isNeedLog {
		log += fmt.Sprintf(` + FixAmount [%v]`, configuration.FixAmount)
	}

	roundedFee, log := b.roundingService.RoundValue(ctx, sourceCurrency, totalConversionFee, log, isNeedLog)

	if isNeedLog {
		sumLog = fmt.Sprintf(`%v`, price) + ` + ` + log
	}

	// Это полная сумма в исходной валюте платежа. Ее необходимо перевести в целевую,
	// перемножив на курсы.
	roundedTotalSum, sumLog := b.roundingService.RoundValue(ctx, sourceCurrency, roundedFee+price, sumLog, isNeedLog)

	totalConvertedSum, totalConvertedSumLog := b.currencyConverter.Convert(
		ctx,
		sourceCurrency,
		targetCurrency,
		roundedTotalSum,
		isNeedLog,
	)

	if isNeedLog {
		sumLog = "(" + sumLog + ") => " + totalConvertedSumLog
	}

	totalConvertedConversion, totalConvertedConversionLog := b.currencyConverter.Convert(
		ctx,
		sourceCurrency,
		targetCurrency,
		roundedFee,
		isNeedLog,
	)

	if isNeedLog {
		log = "(" + log + ") => " + totalConvertedConversionLog
	}

	return totalConvertedConversion, totalConvertedSum, log, sumLog
}
