package conversion_fee

// TaxEntity описывает сущность налога для расчета стоимости
type TaxEntity interface {
	// GetTaxValue возвращает значение налога в процентах
	GetTaxValue() int
}
