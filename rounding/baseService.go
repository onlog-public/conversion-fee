package rounding

import (
	"context"
	"fmt"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"math"
)

// service реализует сервис для работы с округлениями значений
type service struct{}

// RoundValue выполняет округление значения по переданным параметрам
// сущности. При передаче флага логирования, вторым результатом будет
// возвращаться лог округления значения.
func (s service) RoundValue(
	ctx context.Context,
	roundingEntity Entity,
	value float64,
	logValue string,
	isNeedLog bool,
) (float64, string) {
	if h.IsCtxDone(ctx) {
		return 0, ""
	}

	configuration := roundingEntity.GetRoundingConfiguration()
	accuracy := math.Pow10(int(configuration.Accuracy))
	valueWithAccuracy := value * accuracy
	log := ``

	switch configuration.Rule {
	case ToUpper:
		result := math.Ceil(valueWithAccuracy) / accuracy
		if isNeedLog {
			log = fmt.Sprintf(`Rounding [ To Higher([ %v ], %v) = %v ]`, logValue, configuration.Accuracy, result)
		}

		return result, log
	case ToLower:
		result := math.Floor(valueWithAccuracy) / accuracy
		if isNeedLog {
			log = fmt.Sprintf(`Rounding [ To Lower([ %v ], %v) = %v ]`, logValue, configuration.Accuracy, result)
		}

		return result, log
	case ByMath:
		result := math.Round(valueWithAccuracy) / accuracy
		if isNeedLog {
			log = fmt.Sprintf(`Rounding [ By Math([ %v ], %v) = %v ]`, logValue, configuration.Accuracy, result)
		}

		return result, log
	}

	return 0, ""
}

// New реализует конструктор сервиса
func New() Service {
	return &service{}
}
