package rounding

// Entity описывает интерфейс сущности,
type Entity interface {
	// GetRoundingConfiguration возвращает параметры конфигурации округления
	// значений.
	GetRoundingConfiguration() Configuration
}
