package rounding

type Rule int64

const (
	ToUpper Rule = 1 // В большую сторону
	ToLower Rule = 2 // В меньшую сторону
	ByMath  Rule = 3 // По математике
)

// Configuration параметры округления значений
type Configuration struct {
	Accuracy int64 // Точность округления, знаков после запятой
	Rule     Rule  // Правило округления
}
