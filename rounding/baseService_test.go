package rounding

import (
	"context"
	"testing"
)

func Test_service_RoundValue(t *testing.T) {
	type args struct {
		ctx            context.Context
		roundingEntity Entity
		value          float64
		logValue       string
		isNeedLog      bool
	}
	tests := []struct {
		name  string
		args  args
		want  float64
		want1 string
	}{
		{
			name: "Тестирование округления значений в большую сторону",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ToUpper,
					},
				},
				value:     2.01,
				logValue:  "2.01",
				isNeedLog: true,
			},
			want:  2.1,
			want1: "Rounding [ To Higher([ 2.01 ], 1) = 2.1 ]",
		},
		{
			name: "Тестирование округления значений в большую сторону",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ToUpper,
					},
				},
				value:     2.01,
				logValue:  "2.01",
				isNeedLog: false,
			},
			want:  2.1,
			want1: "",
		},
		{
			name: "Тестирование округления значений в меньшую сторону",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ToLower,
					},
				},
				value:     2.09,
				logValue:  "2.09",
				isNeedLog: true,
			},
			want:  2,
			want1: "Rounding [ To Lower([ 2.09 ], 1) = 2 ]",
		},
		{
			name: "Тестирование округления значений в меньшую сторону",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ToLower,
					},
				},
				value:     2.09,
				logValue:  "2.09",
				isNeedLog: false,
			},
			want:  2,
			want1: "",
		},
		{
			name: "Тестирование округления значений по математике",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ByMath,
					},
				},
				value:     2.09,
				logValue:  "2.09",
				isNeedLog: true,
			},
			want:  2.1,
			want1: "Rounding [ By Math([ 2.09 ], 1) = 2.1 ]",
		},
		{
			name: "Тестирование округления значений по математике",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ByMath,
					},
				},
				value:     2.01,
				logValue:  "2.01",
				isNeedLog: true,
			},
			want:  2,
			want1: "Rounding [ By Math([ 2.01 ], 1) = 2 ]",
		},
		{
			name: "Тестирование округления значений по математике",
			args: args{
				ctx: context.Background(),
				roundingEntity: TestEntity{
					Configuration: Configuration{
						Accuracy: 1,
						Rule:     ByMath,
					},
				},
				value:     2.09,
				logValue:  "2.09",
				isNeedLog: false,
			},
			want:  2.1,
			want1: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{}
			got, got1 := s.RoundValue(tt.args.ctx, tt.args.roundingEntity, tt.args.value, tt.args.logValue, tt.args.isNeedLog)
			if got != tt.want {
				t.Errorf("RoundValue() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("RoundValue() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
