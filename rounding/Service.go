package rounding

import "context"

// Service описывает интерфейс сервиса для работы с округлениями значений
type Service interface {
	// RoundValue выполняет округление значения по переданным параметрам
	// сущности. При передаче флага логирования, вторым результатом будет
	// возвращаться лог округления значения.
	RoundValue(
		ctx context.Context,
		roundingEntity Entity,
		value float64,
		logValue string,
		isNeedLog bool,
	) (float64, string)
}
