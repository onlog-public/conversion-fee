package rounding

// TestEntity реализует подставку для тестирования округления
type TestEntity struct {
	Configuration Configuration
}

// GetRoundingConfiguration возвращает параметры конфигурации округления
// значений.
func (t TestEntity) GetRoundingConfiguration() Configuration {
	return t.Configuration
}
