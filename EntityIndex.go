package conversion_fee

import "context"

// EntityIndex описывает интерфейс индекса по сущностям для системы
// расчета стоимости.
type EntityIndex[T any] interface {
	// GetByEntityID возвращает целевую сущность по переданному идентификатору.
	GetByEntityID(ctx context.Context, id string) (T, error)
}
