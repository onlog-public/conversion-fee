package conversion_fee

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"testing"
)

func Test_baseConversionCalculator_CalculateConversion(t *testing.T) {
	type fields struct {
		roundingService   rounding.Service
		currencyConverter currencyConverter
	}
	type args struct {
		ctx            context.Context
		sourceCurrency CurrencyEntity
		targetCurrency CurrencyEntity
		configuration  ConversionConfiguration
		price          float64
		isNeedLog      bool
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   float64
		want1  float64
		want2  string
		want3  string
	}{
		{
			name: "Тестирование расчета конверсионных издержек, когда границы не заданы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           5,
					FixAmount:         0,
					MinValue:          0,
					MaxValue:          0,
					IsMinValueLimited: false,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  50,
			want1: 1050,
			want2: "(Percent amount [ 1000 / 100 * 5 ] + FixAmount [0]) => 50",
			want3: "(1000 + Percent amount [ 1000 / 100 * 5 ] + FixAmount [0]) => 1050",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда границы не заданы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           0,
					FixAmount:         100,
					MinValue:          0,
					MaxValue:          0,
					IsMinValueLimited: false,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  100,
			want1: 1100,
			want2: "(Percent amount [ 1000 / 100 * 0 ] + FixAmount [100]) => 100",
			want3: "(1000 + Percent amount [ 1000 / 100 * 0 ] + FixAmount [100]) => 1100",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда границы не заданы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           5,
					FixAmount:         100,
					MinValue:          0,
					MaxValue:          0,
					IsMinValueLimited: false,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  150,
			want1: 1150,
			want2: "(Percent amount [ 1000 / 100 * 5 ] + FixAmount [100]) => 150",
			want3: "(1000 + Percent amount [ 1000 / 100 * 5 ] + FixAmount [100]) => 1150",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда границы не заданы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           5,
					FixAmount:         100,
					MinValue:          0,
					MaxValue:          0,
					IsMinValueLimited: false,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: false,
			},
			want:  150,
			want1: 1150,
			want2: "",
			want3: "",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда издержки меньше нижней границы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           5,
					FixAmount:         100,
					MinValue:          200,
					MaxValue:          0,
					IsMinValueLimited: true,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  300,
			want1: 1300,
			want2: "(MIN[Percent amount [ 1000 / 100 * 5 ], min :: 200] + FixAmount [100]) => 300",
			want3: "(1000 + MIN[Percent amount [ 1000 / 100 * 5 ], min :: 200] + FixAmount [100]) => 1300",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда издержки меньше нижней границы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           5,
					FixAmount:         100,
					MinValue:          200,
					MaxValue:          0,
					IsMinValueLimited: true,
					IsMaxValueLimited: false,
				},
				price:     1000,
				isNeedLog: false,
			},
			want:  300,
			want1: 1300,
			want2: "",
			want3: "",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда издержки больше верхней границы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           15,
					FixAmount:         100,
					MinValue:          0,
					MaxValue:          100,
					IsMinValueLimited: false,
					IsMaxValueLimited: true,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  200,
			want1: 1200,
			want2: "(MAX[Percent amount [ 1000 / 100 * 15 ], max :: 100] + FixAmount [100]) => 200",
			want3: "(1000 + MAX[Percent amount [ 1000 / 100 * 15 ], max :: 100] + FixAmount [100]) => 1200",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда издержки больше верхней границы",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           15,
					FixAmount:         100,
					MinValue:          0,
					MaxValue:          100,
					IsMinValueLimited: false,
					IsMaxValueLimited: true,
				},
				price:     1000,
				isNeedLog: false,
			},
			want:  200,
			want1: 1200,
			want2: "",
			want3: "",
		},
		{
			name: "Тестирование расчета конверсионных издержек, когда издержки между границами",
			fields: fields{
				roundingService: &roundingServiceMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						``: {``: 1},
					},
				},
			},
			args: args{
				ctx:            context.Background(),
				sourceCurrency: &CurrencyEntityMock{},
				targetCurrency: &CurrencyEntityMock{},
				configuration: ConversionConfiguration{
					Percent:           15,
					FixAmount:         100,
					MinValue:          100,
					MaxValue:          300,
					IsMinValueLimited: true,
					IsMaxValueLimited: true,
				},
				price:     1000,
				isNeedLog: true,
			},
			want:  250,
			want1: 1250,
			want2: "(Percent amount [ 1000 / 100 * 15 ] + FixAmount [100]) => 250",
			want3: "(1000 + Percent amount [ 1000 / 100 * 15 ] + FixAmount [100]) => 1250",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseConversionCalculator{
				roundingService:   tt.fields.roundingService,
				currencyConverter: tt.fields.currencyConverter,
			}
			got, got1, got2, got3 := b.CalculateConversion(tt.args.ctx, tt.args.sourceCurrency, tt.args.targetCurrency, tt.args.configuration, tt.args.price, tt.args.isNeedLog)
			assert.Equalf(t, tt.want, got, "CalculateConversion(%v, %v, %v, %v, %v, %v)", tt.args.ctx, tt.args.sourceCurrency, tt.args.targetCurrency, tt.args.configuration, tt.args.price, tt.args.isNeedLog)
			assert.Equalf(t, tt.want1, got1, "CalculateConversion(%v, %v, %v, %v, %v, %v)", tt.args.ctx, tt.args.sourceCurrency, tt.args.targetCurrency, tt.args.configuration, tt.args.price, tt.args.isNeedLog)
			assert.Equalf(t, tt.want2, got2, "CalculateConversion(%v, %v, %v, %v, %v, %v)", tt.args.ctx, tt.args.sourceCurrency, tt.args.targetCurrency, tt.args.configuration, tt.args.price, tt.args.isNeedLog)
			assert.Equalf(t, tt.want3, got3, "CalculateConversion(%v, %v, %v, %v, %v, %v)", tt.args.ctx, tt.args.sourceCurrency, tt.args.targetCurrency, tt.args.configuration, tt.args.price, tt.args.isNeedLog)
		})
	}
}
