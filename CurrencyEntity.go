package conversion_fee

import "gitlab.com/onlog-public/conversion-fee/rounding"

// CurrencyEntity описывает интерфейс сущности валюты
type CurrencyEntity interface {
	rounding.Entity

	// GetPrimaryKey возвращает первичный ключ валюты
	GetPrimaryKey() string

	// GetCourse курс конвертации валюты.
	GetCourse() float64

	// GetNominal возвращает номинал, за который установлен курс
	GetNominal() float64

	// IsCurrencyAvailableForSource возвращает флаг того, что валюта доступна
	// для выбора в качестве источника
	IsCurrencyAvailableForSource() bool

	// IsCurrencyAvailableForContractors возвращает флаг того, что валюта
	// доступна для расчетов с подрядчиками
	IsCurrencyAvailableForContractors() bool
}
