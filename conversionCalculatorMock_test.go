package conversion_fee

import (
	"context"
	"fmt"
)

// conversionCalculatorMock реализует подставку для тестирования
type conversionCalculatorMock struct{}

// CalculateConversion вычисляет процент конверсионных издержек
// для переданных параметров конвертации. Возвращает округленное
// значение конверсионных издержек и суммы с конверсией
func (c conversionCalculatorMock) CalculateConversion(
	_ context.Context,
	_ CurrencyEntity,
	_ CurrencyEntity,
	configuration ConversionConfiguration,
	price float64,
	_ bool,
) (float64, float64, string, string) {
	conversionFee := price * float64(configuration.Percent) / 100
	result := price + conversionFee

	return conversionFee, result, fmt.Sprintf(`%v`, conversionFee), fmt.Sprintf(`%v`, result)
}
