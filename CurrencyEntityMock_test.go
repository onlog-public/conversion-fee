package conversion_fee

import (
	"gitlab.com/onlog-public/conversion-fee/rounding"
)

// CurrencyEntityMock реализует подставку для тестирования
type CurrencyEntityMock struct {
	Configuration                   rounding.Configuration
	PrimaryKey                      string
	Course                          float64
	Nominal                         float64
	CurrencyAvailableForSource      bool
	CurrencyAvailableForContractors bool
}

// IsCurrencyAvailableForSource возвращает флаг того, что валюта доступна
// для выбора в качестве источника
func (t CurrencyEntityMock) IsCurrencyAvailableForSource() bool {
	return t.CurrencyAvailableForSource
}

// IsCurrencyAvailableForContractors возвращает флаг того, что валюта
// доступна для расчетов с подрядчиками
func (t CurrencyEntityMock) IsCurrencyAvailableForContractors() bool {
	return t.CurrencyAvailableForContractors
}

// GetRoundingConfiguration возвращает параметры конфигурации округления
// значений.
func (t CurrencyEntityMock) GetRoundingConfiguration() rounding.Configuration {
	return t.Configuration
}

// GetPrimaryKey возвращает первичный ключ валюты
func (t CurrencyEntityMock) GetPrimaryKey() string {
	return t.PrimaryKey
}

// GetCourse курс конвертации валюты.
func (t CurrencyEntityMock) GetCourse() float64 {
	return t.Course
}

// GetNominal возвращает номинал, за который установлен курс
func (t CurrencyEntityMock) GetNominal() float64 {
	return t.Nominal
}
