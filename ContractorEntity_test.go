package conversion_fee

// ContractorEntityMock реализует подставку для тестирования
type ContractorEntityMock struct {
	ID string
}

// GetPrimaryKey возвращает первичный ключ подрядчика
func (c ContractorEntityMock) GetPrimaryKey() string {
	return c.ID
}
