package conversion_fee

import "context"

// defaultConversionCalculator описывает интерфейс сервиса расчета конверсионных
// издержек по умолчанию для переданной валютной пары.
type defaultConversionCalculator[T CurrencyEntity] interface {
	// Calculate вычисляет конверсионные издержки по умолчанию для переданной
	// валютной пары.
	Calculate(
		ctx context.Context,
		value float64,
		source T,
		target T,
		conversionPercent int64,
		isNeedLog bool,
	) (ConversionChainCalculationResult, []string)
}
