package conversion_fee

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"sort"
)

// tmpResult описывает результаты расчетов для перевода валют
type tmpResult struct {
	Result ConversionChainCalculationResult
	Log    []string
}

// baseConverter реализует функционала расчета конверсионных
// издержек и конвертации валюты
type baseConverter[T CurrencyEntity, C ContractorEntity] struct {
	conversionChainCalculator   conversionChainCalculator[T, C]
	defaultConversionCalculator defaultConversionCalculator[T]
}

// NewConverter реализует конструктор сервиса конверсионных издержек
func NewConverter[T CurrencyEntity, C ContractorEntity](
	currencyIndex EntityIndex[T],
	systemSettingsIndex ConversionIndex,
	contractorIndex ContractorConversionIndex[C],
) Converter[T, C] {
	return &baseConverter[T, C]{
		conversionChainCalculator: newBaseConversionChainGetter[T, C](
			currencyIndex,
			systemSettingsIndex,
			contractorIndex,
		),
		defaultConversionCalculator: newBaseDefaultConversionCalculator[T](),
	}
}

// Convert выполняет конвертацию переданного значения value из
// исходной валюты source в целевую - target. Метод рассчитывает
// не только значение в целевой валюте, но и рассчитывает конверсионные
// издержки.
// Если передан флаг isNeedLog, то метод возвращает лог преобразования
// значений при конвертации.
// source - валюта ставки подрядчика
// target - целевая валюта пользователя
func (b baseConverter[T, C]) Convert(
	ctx context.Context,
	source T,
	target T,
	contractor C,
	value float64,
	defaultConversionPercent int64,
	isNeedLog bool,
) (*ConversionChainCalculationResult, []string, error) {
	if h.IsCtxDone(ctx) {
		return nil, nil, nil
	}

	chains, err := b.conversionChainCalculator.GetConversionChain(ctx, source, target, contractor)
	if nil != err {
		return nil, nil, errors.Wrap(err, `failed to load conversion chains`)
	}

	if 0 == len(chains) {
		result, log := b.defaultConversionCalculator.Calculate(
			ctx,
			value,
			source,
			target,
			defaultConversionPercent,
			isNeedLog,
		)

		return &result, log, nil
	}

	// Считаем цепочки
	calculatedChains := h.Map(chains, func(chain conversionChain) tmpResult {
		result, log := b.conversionChainCalculator.CalculateChain(ctx, chain, value, isNeedLog)
		return tmpResult{
			Result: result,
			Log:    log,
		}
	})

	sort.SliceStable(calculatedChains, func(i, j int) bool {
		return calculatedChains[i].Result.ConvertedValue < calculatedChains[j].Result.ConvertedValue
	})

	result := calculatedChains[0]

	return &result.Result, result.Log, nil
}
