package conversion_fee

import (
	"context"
)

// EntityIndexMock описывает подставку для тестирования
type EntityIndexMock[T any] struct {
	ResultMap map[string]T
}

// GetByEntityID возвращает целевую сущность по переданному идентификатору.
func (e EntityIndexMock[T]) GetByEntityID(_ context.Context, id string) (T, error) {
	return e.ResultMap[id], nil
}
