package conversion_fee

import (
	"context"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"testing"
)

func Test_baseTaxCalculator_Calculate(t *testing.T) {
	type fields struct {
		rounding rounding.Service
		taxIndex EntityIndex[TaxEntityMock]
	}
	type args struct {
		ctx           context.Context
		price         float64
		currency      CurrencyEntity
		taxID         string
		isTaxIncluded bool
		isNeedLog     bool
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantPriceWithoutTax float64
		wantTaxResult       float64
		wantLog             string
		wantErr             bool
	}{
		{
			name: "Тестирование когда налог включен в стоимость",
			fields: fields{
				rounding: roundingServiceMock{},
				taxIndex: EntityIndexMock[TaxEntityMock]{ResultMap: map[string]TaxEntityMock{
					`1`: {Amount: 20},
				}},
			},
			args: args{
				ctx:           context.Background(),
				price:         120,
				currency:      CurrencyEntityMock{},
				taxID:         "1",
				isTaxIncluded: true,
				isNeedLog:     false,
			},
			wantPriceWithoutTax: 100,
			wantTaxResult:       20,
			wantLog:             "",
			wantErr:             false,
		},
		{
			name: "Тестирование когда налог включен в стоимость",
			fields: fields{
				rounding: roundingServiceMock{},
				taxIndex: EntityIndexMock[TaxEntityMock]{ResultMap: map[string]TaxEntityMock{
					`1`: {Amount: 20},
				}},
			},
			args: args{
				ctx:           context.Background(),
				price:         120,
				currency:      CurrencyEntityMock{},
				taxID:         "1",
				isTaxIncluded: true,
				isNeedLog:     true,
			},
			wantPriceWithoutTax: 100,
			wantTaxResult:       20,
			wantLog:             "Tax (120 :: 20% [120 * 20% / (100% + 20%) = (price :: 100, tax :: 20)] included)",
			wantErr:             false,
		},
		{
			name: "Тестирование когда налог не включен в стоимость",
			fields: fields{
				rounding: roundingServiceMock{},
				taxIndex: EntityIndexMock[TaxEntityMock]{ResultMap: map[string]TaxEntityMock{
					`1`: {Amount: 20},
				}},
			},
			args: args{
				ctx:           context.Background(),
				price:         100,
				currency:      CurrencyEntityMock{},
				taxID:         "1",
				isTaxIncluded: false,
				isNeedLog:     false,
			},
			wantPriceWithoutTax: 100,
			wantTaxResult:       20,
			wantLog:             "",
			wantErr:             false,
		},
		{
			name: "Тестирование когда налог не включен в стоимость",
			fields: fields{
				rounding: roundingServiceMock{},
				taxIndex: EntityIndexMock[TaxEntityMock]{ResultMap: map[string]TaxEntityMock{
					`1`: {Amount: 20},
				}},
			},
			args: args{
				ctx:           context.Background(),
				price:         100,
				currency:      CurrencyEntityMock{},
				taxID:         "1",
				isTaxIncluded: false,
				isNeedLog:     true,
			},
			wantPriceWithoutTax: 100,
			wantTaxResult:       20,
			wantLog:             "Tax (100 :: 20% [100 / 100% x 20% = 20] not included)",
			wantErr:             false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseTaxCalculator[TaxEntityMock]{
				rounding: tt.fields.rounding,
				taxIndex: tt.fields.taxIndex,
			}
			gotPriceWithoutTax, gotTaxResult, gotLog, err := b.Calculate(tt.args.ctx, tt.args.price, tt.args.currency, tt.args.taxID, tt.args.isTaxIncluded, tt.args.isNeedLog)
			if (err != nil) != tt.wantErr {
				t.Errorf("Calculate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotPriceWithoutTax != tt.wantPriceWithoutTax {
				t.Errorf("Calculate() gotPriceWithoutTax = %v, want %v", gotPriceWithoutTax, tt.wantPriceWithoutTax)
			}
			if gotTaxResult != tt.wantTaxResult {
				t.Errorf("Calculate() gotTaxResult = %v, want %v", gotTaxResult, tt.wantTaxResult)
			}
			if gotLog != tt.wantLog {
				t.Errorf("Calculate() gotLog = %v, want %v", gotLog, tt.wantLog)
			}
		})
	}
}
