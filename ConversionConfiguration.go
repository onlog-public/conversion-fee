package conversion_fee

// ConversionConfiguration описывает параметры расчета конверсионных издержек
// на основе процентного значения и фиксированной ставки
type ConversionConfiguration struct {
	Percent           int64   // Процент конверсионных издержек
	FixAmount         float64 // Фиксированная стоимость конверсионных издержек
	MinValue          float64 // Минимальная стоимость конверсии
	MaxValue          float64 // Максимальная стоимость конверсии
	IsMinValueLimited bool    // Минимальная стоимость ограничена
	IsMaxValueLimited bool    // Максимальная стоимость ограничена
}
