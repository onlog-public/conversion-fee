package conversion_fee

import "context"

// Converter описывает интерфейс конвертера конверсионных издержек.
// Сервис переводит валюту из исходной в целевую с расчетом конверсионных
// издержек.
type Converter[T CurrencyEntity, C ContractorEntity] interface {
	// Convert выполняет конвертацию переданного значения value из
	// исходной валюты source в целевую - target. Метод рассчитывает
	// не только значение в целевой валюте, но и рассчитывает конверсионные
	// издержки.
	// Если передан флаг isNeedLog, то метод возвращает лог преобразования
	// значений при конвертации.
	// source - валюта ставки подрядчика
	// target - целевая валюта пользователя
	Convert(
		ctx context.Context,
		source T,
		target T,
		contractor C,
		value float64,
		defaultConversionPercent int64,
		isNeedLog bool,
	) (*ConversionChainCalculationResult, []string, error)
}
