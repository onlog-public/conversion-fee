package conversion_fee

import (
	"context"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"reflect"
	"testing"
)

func Test_baseConverter_Convert(t *testing.T) {
	type fields struct {
		conversionChainCalculator       conversionChainCalculator[CurrencyEntityMock, ContractorEntityMock]
		defaultConversionCalculatorMock defaultConversionCalculatorMock[CurrencyEntityMock]
	}
	type args struct {
		ctx                      context.Context
		source                   CurrencyEntityMock
		target                   CurrencyEntityMock
		contractor               ContractorEntityMock
		value                    float64
		defaultConversionPercent int64
		isNeedLog                bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *ConversionChainCalculationResult
		want1   []string
		wantErr bool
	}{
		{
			name: "Тестирование расчета и выбора наиболее дешевой ставки",
			fields: fields{
				defaultConversionCalculatorMock: defaultConversionCalculatorMock[CurrencyEntityMock]{},
				conversionChainCalculator: conversionChainCalculatorMock[CurrencyEntityMock, ContractorEntityMock]{
					Result: []conversionChainWithResult{
						{
							conversionChain: conversionChain{
								SystemConversion: chainConfiguration{
									Source: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "1",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
									Target: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "2",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
								},
								ContractorConversion: chainConfiguration{
									Source: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "2",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
									Target: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "3",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
								},
							},
							ConversionChainCalculationResult: ConversionChainCalculationResult{
								BaseValue:      100,
								ConvertedValue: 1000,
								ContractorFee:  100,
								SystemFee:      100,
								InnerCurrency:  "1",
							},
						},
						{
							conversionChain: conversionChain{
								SystemConversion: chainConfiguration{
									Source: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "3",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
									Target: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "4",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
								},
								ContractorConversion: chainConfiguration{
									Source: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "4",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
									Target: CurrencyEntityMock{
										Configuration: rounding.Configuration{
											Accuracy: 2,
											Rule:     rounding.ByMath,
										},
										PrimaryKey:                      "5",
										Course:                          200,
										Nominal:                         1,
										CurrencyAvailableForSource:      true,
										CurrencyAvailableForContractors: true,
									},
								},
							},
							ConversionChainCalculationResult: ConversionChainCalculationResult{
								BaseValue:      100,
								ConvertedValue: 2000,
								ContractorFee:  200,
								SystemFee:      200,
								InnerCurrency:  "2",
							},
						},
					},
				},
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor:               ContractorEntityMock{},
				value:                    100,
				defaultConversionPercent: 0,
				isNeedLog:                false,
			},
			want: &ConversionChainCalculationResult{
				BaseValue:      100,
				ConvertedValue: 1000,
				ContractorFee:  100,
				SystemFee:      100,
				InnerCurrency:  "1",
			},
			want1:   nil,
			wantErr: false,
		},
		{
			name: "Тестирование расчета издержек по умолчанию, когда не найдены цепочки конвертации",
			fields: fields{
				defaultConversionCalculatorMock: defaultConversionCalculatorMock[CurrencyEntityMock]{
					Result: ConversionChainCalculationResult{
						BaseValue:      100,
						ConvertedValue: 1,
						ContractorFee:  2,
						SystemFee:      3,
						InnerCurrency:  "0",
					},
				},
				conversionChainCalculator: conversionChainCalculatorMock[CurrencyEntityMock, ContractorEntityMock]{
					Result: []conversionChainWithResult{},
				},
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor:               ContractorEntityMock{},
				value:                    100,
				defaultConversionPercent: 0,
				isNeedLog:                false,
			},
			want: &ConversionChainCalculationResult{
				BaseValue:      100,
				ConvertedValue: 1,
				ContractorFee:  2,
				SystemFee:      3,
				InnerCurrency:  "0",
			},
			want1:   nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseConverter[CurrencyEntityMock, ContractorEntityMock]{
				conversionChainCalculator:   tt.fields.conversionChainCalculator,
				defaultConversionCalculator: tt.fields.defaultConversionCalculatorMock,
			}
			got, got1, err := b.Convert(tt.args.ctx, tt.args.source, tt.args.target, tt.args.contractor, tt.args.value, tt.args.defaultConversionPercent, tt.args.isNeedLog)
			if (err != nil) != tt.wantErr {
				t.Errorf("Convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Convert() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
