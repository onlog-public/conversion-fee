package conversion_fee

import (
	"context"
	"fmt"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// baseCurrencyConverter реализует конвертер currencyConverter
type baseCurrencyConverter struct {
	roundingService rounding.Service
}

// newBaseCurrencyConverter реализует конструктор конвертера
func newBaseCurrencyConverter() currencyConverter {
	return &baseCurrencyConverter{
		roundingService: rounding.New(),
	}
}

// Convert выполняет конвертацию переданного значения из
// переданной валюты в целевую.
func (b baseCurrencyConverter) Convert(
	ctx context.Context,
	source CurrencyEntity,
	target CurrencyEntity,
	sourceValue float64,
	isNeedLog bool,
) (float64, string) {
	if h.IsCtxDone(ctx) {
		return 0, ""
	}

	sourceLogValue := ``
	if isNeedLog {
		sourceLogValue = fmt.Sprintf(`%v`, sourceValue)
	}

	roundedSource, sourceLogValue := b.roundingService.RoundValue(ctx, source, sourceValue, sourceLogValue, isNeedLog)

	convertedValue := roundedSource * source.GetCourse() / source.GetNominal() * target.GetNominal() / target.GetCourse()
	convertedValueLog := ``
	if isNeedLog {
		convertedValueLog = fmt.Sprintf(
			`%v × %v / %v / %v × %v`,
			sourceLogValue,
			source.GetCourse(),
			source.GetNominal(),
			target.GetCourse(),
			target.GetNominal(),
		)
	}

	return b.roundingService.RoundValue(
		ctx,
		target,
		convertedValue,
		convertedValueLog,
		isNeedLog,
	)
}
