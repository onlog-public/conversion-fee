package conversion_fee

import (
	"context"
)

// ConverterMock реализует подставку для тестирования
type ConverterMock[T CurrencyEntity, C ContractorEntity] struct {
	Result *ConversionChainCalculationResult
}

// Convert выполняет конвертацию переданного значения value из
// исходной валюты source в целевую - target. Метод рассчитывает
// не только значение в целевой валюте, но и рассчитывает конверсионные
// издержки.
// Если передан флаг isNeedLog, то метод возвращает лог преобразования
// значений при конвертации.
// source - валюта ставки подрядчика
// target - целевая валюта пользователя
func (c ConverterMock[T, C]) Convert(
	_ context.Context,
	_ T,
	_ T,
	_ C,
	_ float64,
	_ int64,
	_ bool,
) (*ConversionChainCalculationResult, []string, error) {
	return c.Result, nil, nil
}
