package conversion_fee

import (
	"context"
)

// taxCalculatorMock описывает подставку для тестирования
type taxCalculatorMock struct {
	TaxValue float64
}

// Calculate выполняет расчет налога по переданным параметрам
// и возвращает стоимость без налога, с налогом и сам налог.
// Если в метод передан флаг логирования, то будет вестись лог
// расчета налога.
func (t taxCalculatorMock) Calculate(
	_ context.Context,
	price float64,
	_ CurrencyEntity,
	_ string,
	_ bool,
	_ bool,
) (priceWithoutTax float64, taxResult float64, log string, err error) {
	return price, t.TaxValue, ``, nil
}
