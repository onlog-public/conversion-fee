package conversion_fee

// chainConfiguration описывает параметры звена цепочки
// конфигурации конвертации валюты
type chainConfiguration struct {
	Source   CurrencyEntity
	Target   CurrencyEntity
	Settings EntityWithConversionSettings
}

// conversionChain описывает данные цепочки конвертации валюты
type conversionChain struct {
	SystemConversion     chainConfiguration
	ContractorConversion chainConfiguration
}

// ConversionChainCalculationResult описывает результаты вычисления
// конверсионных издержек для переданной цепочки конверсии.
type ConversionChainCalculationResult struct {
	BaseValue                     float64
	ConvertedValue                float64
	ContractorFee                 float64
	ContractorFeeInSystemCurrency float64
	SystemFee                     float64
	InnerCurrency                 string
}
