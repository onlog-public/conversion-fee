package conversion_fee

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"testing"
)

func Test_baseOfferCalculator_Calculate(t *testing.T) {
	type fields struct {
		taxCalculator taxCalculator
		rounding      rounding.Service
		converter     Converter[CurrencyEntityMock, ContractorEntityMock]
		currencyIndex EntityIndex[CurrencyEntityMock]
		contractorIdx EntityIndex[ContractorEntityMock]
	}
	type args struct {
		ctx       context.Context
		offer     OfferToCalculate
		isNeedLog bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *OfferCalculationResult
		want1   []string
		wantErr bool
	}{
		{
			name: "Тестирование расчета базовой стоимости, когда ограничения не заданы",
			fields: fields{
				taxCalculator: taxCalculatorMock{
					TaxValue: 200,
				},
				rounding: roundingServiceMock{},
				converter: ConverterMock[CurrencyEntityMock, ContractorEntityMock]{
					Result: &ConversionChainCalculationResult{
						BaseValue:                     0,
						ConvertedValue:                1000,
						ContractorFee:                 30,
						ContractorFeeInSystemCurrency: 300,
						SystemFee:                     100,
						InnerCurrency:                 "22",
					},
				},
				currencyIndex: EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							PrimaryKey: "1",
						},
						`2`: {
							PrimaryKey: "2",
						},
					},
				},
				contractorIdx: EntityIndexMock[ContractorEntityMock]{
					ResultMap: map[string]ContractorEntityMock{
						"1": {ID: `1`},
					},
				},
			},
			args: args{
				ctx: context.Background(),
				offer: OfferToCalculate{
					ContractorID:   "1",
					Quantity:       3,
					Price:          100,
					IsFixed:        false,
					IsTaxIncluded:  false,
					MinimalPrice:   0,
					SourceCurrency: "1",
					TargetCurrency: "2",
					Tax:            "33",
				},
				isNeedLog: false,
			},
			want: &OfferCalculationResult{
				Quantity:           3,
				SinglePrice:        100,
				BasePrice:          300,
				BaseTax:            200,
				BaseFullPrice:      500,
				InnerConversionFee: 30,
				InnerCurrency:      "22",
				Price:              1000,
				Tax:                200,
				SystemFee:          100,
				ConversionFee:      400,
				FullPrice:          1200,
			},
			want1:   []string{},
			wantErr: false,
		},
		{
			name: "Тестирование расчета базовой стоимости, когда есть минимальная стоимость",
			fields: fields{
				taxCalculator: taxCalculatorMock{
					TaxValue: 200,
				},
				rounding: roundingServiceMock{},
				converter: ConverterMock[CurrencyEntityMock, ContractorEntityMock]{
					Result: &ConversionChainCalculationResult{
						BaseValue:                     0,
						ConvertedValue:                1000,
						ContractorFee:                 30,
						ContractorFeeInSystemCurrency: 300,
						SystemFee:                     100,
						InnerCurrency:                 "22",
					},
				},
				currencyIndex: EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							PrimaryKey: "1",
						},
						`2`: {
							PrimaryKey: "2",
						},
					},
				},
				contractorIdx: EntityIndexMock[ContractorEntityMock]{
					ResultMap: map[string]ContractorEntityMock{
						"1": {ID: `1`},
					},
				},
			},
			args: args{
				ctx: context.Background(),
				offer: OfferToCalculate{
					ContractorID:   "1",
					Quantity:       3,
					Price:          100,
					IsFixed:        false,
					IsTaxIncluded:  false,
					MinimalPrice:   1200,
					SourceCurrency: "1",
					TargetCurrency: "2",
					Tax:            "33",
				},
				isNeedLog: false,
			},
			want: &OfferCalculationResult{
				Quantity:           1,
				SinglePrice:        1200,
				BasePrice:          1200,
				BaseTax:            200,
				BaseFullPrice:      1400,
				InnerConversionFee: 30,
				InnerCurrency:      "22",
				Price:              1000,
				Tax:                200,
				SystemFee:          100,
				ConversionFee:      400,
				FullPrice:          1200,
			},
			want1:   []string{},
			wantErr: false,
		},
		{
			name: "Тестирование расчета базовой стоимости, когда есть фиксированная стоимость",
			fields: fields{
				taxCalculator: taxCalculatorMock{
					TaxValue: 200,
				},
				rounding: roundingServiceMock{},
				converter: ConverterMock[CurrencyEntityMock, ContractorEntityMock]{
					Result: &ConversionChainCalculationResult{
						BaseValue:                     0,
						ConvertedValue:                1000,
						ContractorFee:                 30,
						ContractorFeeInSystemCurrency: 300,
						SystemFee:                     100,
						InnerCurrency:                 "22",
					},
				},
				currencyIndex: EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							PrimaryKey: "1",
						},
						`2`: {
							PrimaryKey: "2",
						},
					},
				},
				contractorIdx: EntityIndexMock[ContractorEntityMock]{
					ResultMap: map[string]ContractorEntityMock{
						"1": {ID: `1`},
					},
				},
			},
			args: args{
				ctx: context.Background(),
				offer: OfferToCalculate{
					ContractorID:   "1",
					Quantity:       3,
					Price:          500,
					IsFixed:        true,
					IsTaxIncluded:  false,
					MinimalPrice:   1200,
					SourceCurrency: "1",
					TargetCurrency: "2",
					Tax:            "33",
				},
				isNeedLog: false,
			},
			want: &OfferCalculationResult{
				Quantity:           1,
				SinglePrice:        500,
				BasePrice:          500,
				BaseTax:            200,
				BaseFullPrice:      700,
				InnerConversionFee: 30,
				InnerCurrency:      "22",
				Price:              1000,
				Tax:                200,
				SystemFee:          100,
				ConversionFee:      400,
				FullPrice:          1200,
			},
			want1:   []string{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseOfferCalculator[CurrencyEntityMock, ContractorEntityMock]{
				taxCalculator: tt.fields.taxCalculator,
				rounding:      tt.fields.rounding,
				converter:     tt.fields.converter,
				currencyIndex: tt.fields.currencyIndex,
				contractorIdx: tt.fields.contractorIdx,
			}
			got, got1, err := b.Calculate(tt.args.ctx, tt.args.offer, tt.args.isNeedLog)
			if (err != nil) != tt.wantErr {
				t.Errorf("Calculate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, got, tt.want)
			assert.Equal(t, got1, tt.want1)
		})
	}
}
