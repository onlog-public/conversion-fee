package conversion_fee

// EntityWithConversionSettingsMock реализует подставку под тестирование
type EntityWithConversionSettingsMock struct {
	Source        string
	Target        string
	Configuration ConversionConfiguration
}

// GetSourceCurrency возвращает первичный ключ исходной валюты,
// для которой заданы настройки расчета конверсионных издержек
func (e EntityWithConversionSettingsMock) GetSourceCurrency() string {
	return e.Source
}

// GetTargetCurrency возвращает первичный ключ целевой валюты,
// для которой заданы настройки расчета конверсионных издержек
func (e EntityWithConversionSettingsMock) GetTargetCurrency() string {
	return e.Target
}

// GetConversionSettings возвращает параметры конфигурации расчета
// конверсионных издержек.
func (e EntityWithConversionSettingsMock) GetConversionSettings() ConversionConfiguration {
	return e.Configuration
}
