package conversion_fee

import (
	"context"
)

// ConversionIndexMock реализует подставку для тестирования
type ConversionIndexMock struct {
	SourceCurrencySettings []EntityWithConversionSettings
	TargetCurrencySettings []EntityWithConversionSettings
}

// GetSettingsWithSourceCurrency возвращает массив настроек расчета
// конверсионных издержек, которые настроены для переданной исходной
// валюты.
func (c ConversionIndexMock) GetSettingsWithSourceCurrency(_ context.Context, _ string) ([]EntityWithConversionSettings, error) {
	return c.SourceCurrencySettings, nil
}

// GetSettingsWithTargetCurrency возвращает массив настроек расчета
// конверсионных издержек, которые настроены для переданной целевой
// валюты
func (c ConversionIndexMock) GetSettingsWithTargetCurrency(_ context.Context, _ string) ([]EntityWithConversionSettings, error) {
	return c.TargetCurrencySettings, nil
}
