package conversion_fee

import "context"

// OfferCalculator описывает интерфейс калькулятора расчета стоимости
// ценового предложения. Рассчитывает как базовую стоимость и налоги,
// так и конверсионные издержки.
type OfferCalculator[T CurrencyEntity, C ContractorEntity] interface {
	// Calculate выполняет расчет переданного ценового предложения и
	// возвращает результат расчета.
	// Если передан флаг isNeedLog, то в результатах расчета будет
	// возвращаться лог расчета стоимости.
	Calculate(
		ctx context.Context,
		offer OfferToCalculate,
		isNeedLog bool,
	) (*OfferCalculationResult, []string, error)
}
