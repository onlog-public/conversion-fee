package conversion_fee

import (
	"context"
	"fmt"
)

// currencyConverterMock реализует подставку для тестирования
type currencyConverterMock struct {
	Coefficient map[string]map[string]float64
}

// Convert выполняет конвертацию переданного значения из
// переданной валюты в целевую.
func (c currencyConverterMock) Convert(
	_ context.Context,
	source CurrencyEntity,
	target CurrencyEntity,
	sourceValue float64,
	_ bool,
) (float64, string) {
	coefficients, ok := c.Coefficient[source.GetPrimaryKey()]
	if !ok {
		return 0, ``
	}

	coefficient, ok := coefficients[target.GetPrimaryKey()]
	if !ok {
		return 0, ``
	}

	result := sourceValue * coefficient

	return result, fmt.Sprintf(`%v`, result)
}
