package conversion_fee

// TaxEntityMock реализует подставку для тестирования
type TaxEntityMock struct {
	Amount int
}

// GetTaxValue возвращает значение налога в процентах
func (t TaxEntityMock) GetTaxValue() int {
	return t.Amount
}
