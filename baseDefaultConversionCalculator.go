package conversion_fee

import (
	"context"
)

// baseDefaultConversionCalculator реализует сервис расчета конверсионных
// издержек по умолчанию для переданной валютной пары.
type baseDefaultConversionCalculator[T CurrencyEntity] struct {
	conversionCalculator conversionCalculator
}

// newBaseDefaultConversionCalculator реализует конструктор сервиса
func newBaseDefaultConversionCalculator[T CurrencyEntity]() defaultConversionCalculator[T] {
	return &baseDefaultConversionCalculator[T]{
		conversionCalculator: newBaseConversionCalculator(),
	}
}

// Calculate вычисляет конверсионные издержки по умолчанию для переданной
// валютной пары.
func (b baseDefaultConversionCalculator[T]) Calculate(
	ctx context.Context,
	value float64,
	source T,
	target T,
	conversionPercent int64,
	isNeedLog bool,
) (ConversionChainCalculationResult, []string) {
	var resultLog []string

	// Теперь необходимо вычислить конверсионные издержки пользователя.
	systemValueFee, systemValue, systemValueFeeLog, systemValueLog := b.conversionCalculator.CalculateConversion(
		ctx,
		source,
		target,
		ConversionConfiguration{
			Percent:           conversionPercent,
			FixAmount:         0,
			MinValue:          0,
			MaxValue:          0,
			IsMinValueLimited: false,
			IsMaxValueLimited: false,
		},
		value,
		isNeedLog,
	)

	if isNeedLog && 0 != len(systemValueFeeLog) {
		resultLog = append(resultLog, `Calculating default conversion fee: `+systemValueFeeLog)
	}

	if isNeedLog && 0 != len(systemValueLog) {
		resultLog = append(resultLog, `Calculation default total payment: `+systemValueLog)
	}

	return ConversionChainCalculationResult{
		BaseValue:      value,
		ConvertedValue: systemValue,
		ContractorFee:  0,
		SystemFee:      systemValueFee,
		InnerCurrency:  "0",
	}, resultLog
}
