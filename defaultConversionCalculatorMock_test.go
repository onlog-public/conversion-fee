package conversion_fee

import (
	"context"
)

// defaultConversionCalculatorMock реализует подставку для тестирования
type defaultConversionCalculatorMock[T CurrencyEntity] struct {
	Result ConversionChainCalculationResult
	Log    []string
}

// Calculate вычисляет конверсионные издержки по умолчанию для переданной
// валютной пары.
func (d defaultConversionCalculatorMock[T]) Calculate(
	_ context.Context,
	_ float64,
	_ T,
	_ T,
	_ int64,
	_ bool,
) (ConversionChainCalculationResult, []string) {
	return d.Result, d.Log
}
