package conversion_fee

// OfferToCalculate описывает DTO для передачи данных для расчета
// стоимости одного ценового предложения.
type OfferToCalculate struct {
	ContractorID      string  // Идентификатор подрядчика, к которому относится ЦП
	Quantity          float64 // Количество для расчета
	Price             float64 // Цена предложения
	IsFixed           bool    // Стоимость фиксирована
	IsTaxIncluded     bool    // Налог уже включен в стоимость ЦП
	MinimalPrice      float64 // Минимальная стоимость
	SourceCurrency    string  // Валюта ЦП
	TargetCurrency    string  // Валюта, в которой запрошена оплата
	Tax               string  // Налог, который собирается с ЦП
	DefaultConversion int64   // Процент конверсионных по умолчанию, когда нет цепочек конверсии
}

// OfferCalculationResult описывает результаты расчета стоимости одного
// ценового предложения.
type OfferCalculationResult struct {
	Quantity           float64 // Итоговое количество, которое пошло в расчет
	SinglePrice        float64 // Базовая стоимость в исходной валюте ЦП за единицу товара
	BasePrice          float64 // Базовая стоимость в исходной валюте ЦП
	BaseTax            float64 // Налог на базовую стоимость в исходной валюте ЦП
	BaseFullPrice      float64 // Полная базовая стоимость в исходной валюте ЦП
	InnerConversionFee float64 // Внутренние конверсионные издержки, которые идут на оплату подрядчику
	InnerCurrency      string  // Внутренняя валюта, в которой требуется выполнять расчеты с подрядчиком
	Price              float64 // Стоимость ЦП в целевой валюте
	Tax                float64 // Налог на ЦП в целевой валюте
	SystemFee          float64 // Системные конверсионные издержки
	ConversionFee      float64 // Полные конверсионные издержки, возникающие при оплате
	FullPrice          float64 // Полная стоимость ЦП в целевой валюте, включая налоги и конверсионные издержки
}
