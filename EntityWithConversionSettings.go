package conversion_fee

// EntityWithConversionSettings описывает интерфейс сущности, содержащей
// параметры расчета конверсионных издержек
type EntityWithConversionSettings interface {
	// GetSourceCurrency возвращает первичный ключ исходной валюты,
	// для которой заданы настройки расчета конверсионных издержек
	GetSourceCurrency() string

	// GetTargetCurrency возвращает первичный ключ целевой валюты,
	// для которой заданы настройки расчета конверсионных издержек
	GetTargetCurrency() string

	// GetConversionSettings возвращает параметры конфигурации расчета
	// конверсионных издержек.
	GetConversionSettings() ConversionConfiguration
}
