package conversion_fee

import "context"

// currencyConverter описывает интерфейс конвертера валюты
type currencyConverter interface {
	// Convert выполняет конвертацию переданного значения из
	// переданной валюты в целевую.
	Convert(
		ctx context.Context,
		source CurrencyEntity,
		target CurrencyEntity,
		sourceValue float64,
		isNeedLog bool,
	) (float64, string)
}
