package conversion_fee

import (
	"context"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"math"
)

// roundingServiceMock реализует подставку для тестирования вычисления
// конверсионных издержек
type roundingServiceMock struct{}

// RoundValue выполняет округление значения по переданным параметрам
// сущности. При передаче флага логирования, вторым результатом будет
// возвращаться лог округления значения.
func (r roundingServiceMock) RoundValue(
	_ context.Context,
	_ rounding.Entity,
	value float64,
	logValue string,
	_ bool,
) (float64, string) {
	return math.Round(value), logValue
}
