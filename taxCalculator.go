package conversion_fee

import "context"

// taxCalculator описывает интерфейс сервиса расчета налогов
type taxCalculator interface {
	// Calculate выполняет расчет налога по переданным параметрам
	// и возвращает стоимость без налога, с налогом и сам налог.
	// Если в метод передан флаг логирования, то будет вестись лог
	// расчета налога.
	Calculate(
		ctx context.Context,
		price float64,
		currency CurrencyEntity,
		taxID string,
		isTaxIncluded bool,
		isNeedLog bool,
	) (priceWithoutTax float64, taxResult float64, log string, err error)
}
