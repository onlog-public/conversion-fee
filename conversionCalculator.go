package conversion_fee

import (
	"context"
)

// conversionCalculator описывает интерфейс сервиса округления значений
// по переданным параметрам конверсии.
type conversionCalculator interface {
	// CalculateConversion вычисляет процент конверсионных издержек
	// для переданных параметров конвертации. Возвращает округленное
	// значение конверсионных издержек и суммы с конверсией
	CalculateConversion(
		ctx context.Context,
		sourceCurrency CurrencyEntity,
		targetCurrency CurrencyEntity,
		configuration ConversionConfiguration,
		price float64,
		isNeedLog bool,
	) (float64, float64, string, string)
}
