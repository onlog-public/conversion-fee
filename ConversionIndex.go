package conversion_fee

import "context"

// ContractorConversionIndex описывает геттер индекса по подрядчику
type ContractorConversionIndex[C ContractorEntity] func(contractor C) (ConversionIndex, error)

// ConversionIndex описывает интерфейс индекса для получения
// настроек расчета конверсионных издержек.
type ConversionIndex interface {
	// GetSettingsWithSourceCurrency возвращает массив настроек расчета
	// конверсионных издержек, которые настроены для переданной исходной
	// валюты.
	GetSettingsWithSourceCurrency(
		ctx context.Context,
		sourceCurrency string,
	) ([]EntityWithConversionSettings, error)

	// GetSettingsWithTargetCurrency возвращает массив настроек расчета
	// конверсионных издержек, которые настроены для переданной целевой
	// валюты
	GetSettingsWithTargetCurrency(
		ctx context.Context,
		sourceCurrency string,
	) ([]EntityWithConversionSettings, error)
}
