package conversion_fee

import "context"

// conversionChainCalculator описывает интерфейс генератора цепочек конвертации,
// доступных для расчета стоимости.
type conversionChainCalculator[T CurrencyEntity, C ContractorEntity] interface {
	// GetConversionChain возвращает наборы подходящих для конвертации цепочек.
	// source - валюта ставки подрядчика
	// target - целевая валюта пользователя
	GetConversionChain(
		ctx context.Context,
		source T,
		target T,
		contractor C,
	) ([]conversionChain, error)

	// CalculateChain вычисляет стоимость переданной цепочки конверсии.
	// Возвращает полностью рассчитанную и переведенную стоимость по этой цепочке.
	CalculateChain(
		ctx context.Context,
		chain conversionChain,
		value float64,
		isNeedLog bool,
	) (ConversionChainCalculationResult, []string)
}
