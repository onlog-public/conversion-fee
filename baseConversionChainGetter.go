package conversion_fee

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// baseConversionChainGetter реализует генератор цепочек конвертации
// валют conversionChainCalculator.
type baseConversionChainGetter[T CurrencyEntity, C ContractorEntity] struct {
	currencyIndex        EntityIndex[T]
	systemSettingsIndex  ConversionIndex
	contractorIndex      ContractorConversionIndex[C]
	conversionCalculator conversionCalculator
	currencyConverter    currencyConverter
}

// newBaseConversionChainGetter реализует конструктор генератора
func newBaseConversionChainGetter[T CurrencyEntity, C ContractorEntity](
	currencyIndex EntityIndex[T],
	systemSettingsIndex ConversionIndex,
	contractorIndex ContractorConversionIndex[C],
) conversionChainCalculator[T, C] {
	return &baseConversionChainGetter[T, C]{
		currencyIndex:        currencyIndex,
		systemSettingsIndex:  systemSettingsIndex,
		contractorIndex:      contractorIndex,
		conversionCalculator: newBaseConversionCalculator(),
		currencyConverter:    newBaseCurrencyConverter(),
	}
}

// CalculateChain вычисляет стоимость переданной цепочки конверсии.
// Возвращает полностью рассчитанную и переведенную стоимость по этой цепочке.
func (b baseConversionChainGetter[T, C]) CalculateChain(
	ctx context.Context,
	chain conversionChain,
	value float64,
	isNeedLog bool,
) (ConversionChainCalculationResult, []string) {
	if h.IsCtxDone(ctx) {
		return ConversionChainCalculationResult{}, nil
	}

	var resultLog []string
	if isNeedLog {
		resultLog = make([]string, 0, 4)
	}

	// Теперь необходимо вычислить конверсионные издержки оплаты подрядчику.
	// Эти издержки будут включаться в общую сумму издержек.
	contractorValueFee, contractorValue, contractorValueFeeLog, contractorValueLog := b.conversionCalculator.CalculateConversion(
		ctx,
		chain.ContractorConversion.Target,
		chain.ContractorConversion.Source,
		chain.ContractorConversion.Settings.GetConversionSettings(),
		value,
		isNeedLog,
	)

	if isNeedLog && 0 != len(contractorValueFeeLog) {
		resultLog = append(resultLog, `Calculating contractors conversion fee: `+contractorValueFeeLog)
	}

	if isNeedLog && 0 != len(contractorValueLog) {
		resultLog = append(resultLog, `Calculation contractor total payment: `+contractorValueLog)
	}

	// Теперь необходимо вычислить конверсионные издержки пользователя.
	systemValueFee, systemValue, systemValueFeeLog, systemValueLog := b.conversionCalculator.CalculateConversion(
		ctx,
		chain.SystemConversion.Target,
		chain.SystemConversion.Source,
		chain.SystemConversion.Settings.GetConversionSettings(),
		contractorValue,
		isNeedLog,
	)

	if isNeedLog && 0 != len(systemValueFeeLog) {
		resultLog = append(resultLog, `Calculating system user conversion fee: `+systemValueFeeLog)
	}

	if isNeedLog && 0 != len(systemValueLog) {
		resultLog = append(resultLog, `Calculation system user total payment: `+systemValueLog)
	}

	contractorFeeInSystemCurrency, _ := b.currencyConverter.Convert(
		ctx,
		chain.SystemConversion.Target,
		chain.SystemConversion.Source,
		contractorValueFee,
		false,
	)

	return ConversionChainCalculationResult{
		BaseValue:                     value,
		ConvertedValue:                systemValue,
		ContractorFee:                 contractorValueFee,
		ContractorFeeInSystemCurrency: contractorFeeInSystemCurrency,
		SystemFee:                     systemValueFee,
		InnerCurrency:                 chain.ContractorConversion.Source.GetPrimaryKey(),
	}, resultLog
}

// GetConversionChain возвращает наборы подходящих для конвертации цепочек.
// source - валюта ставки подрядчика
// target - целевая валюта пользователя
func (b baseConversionChainGetter[T, C]) GetConversionChain(
	ctx context.Context,
	source T,
	target T,
	contractor C,
) ([]conversionChain, error) {
	if h.IsCtxDone(ctx) {
		return nil, nil
	}

	if !source.IsCurrencyAvailableForSource() {
		return nil, errors.New(`currency is not available for source`)
	}

	contractorIndex, err := b.contractorIndex(contractor)
	if nil != err {
		return nil, errors.Wrap(err, `failed to get contractor conversion index`)
	}

	// Пример работы цепочки:
	// Пользователь желает оплатить услуги в RUB, ставка подрядчика в USD.
	// Необходимо принять от пользователя RUB, купить на них EUR, в которых
	// подрядчик принимает оплату по ставке в USD.
	// Необходимо получить все преобразования подрядчика в USD, а так же
	// все преобразования системы из RUB.
	// Далее необходимо состыковать эти преобразования, чтоб целевая валюта
	// преобразования системы была такая же, как и исходная валюта подрядчика.

	// Получаем настройки конвертации для системных конверсионных издержек
	systemSettings, err := b.systemSettingsIndex.GetSettingsWithSourceCurrency(ctx, target.GetPrimaryKey())
	if nil != err {
		return nil, errors.Wrap(err, `failed to get system settings for currency [`+source.GetPrimaryKey()+`]`)
	}

	// Получаем настройки конвертации для издержек подрядчиков
	contractorSettings, err := contractorIndex.GetSettingsWithTargetCurrency(ctx, source.GetPrimaryKey())
	if nil != err {
		return nil, errors.Wrap(err, `failed to get contractor settings for currency [`+target.GetPrimaryKey()+`]`)
	}

	// Формируем идентификаторы валют, которые подрядчик может принимать для оплаты
	contractorSettingsSourceCurrency := h.Map(contractorSettings, func(item EntityWithConversionSettings) string {
		return item.GetSourceCurrency()
	})

	// Заполняем цепочки конвертации для системных издержек.
	// Если не найдена валюта, или настройки не валидны, то они фильтруются.
	availableSystemSettings, err := b.mapSettings(ctx, target, nil, systemSettings)
	if nil != err {
		return nil, errors.Wrap(err, `failed to map system settings`)
	}

	// Оставляем только те системные конвертации, которые ведут к валюте,
	// в которой можно оплачивать подрядчикам.
	filteredSystemSettings := h.Filter(availableSystemSettings, func(item *chainConfiguration) bool {
		if !item.Target.IsCurrencyAvailableForContractors() {
			return false
		}

		return h.InArray(item.Target.GetPrimaryKey(), contractorSettingsSourceCurrency)
	})

	// Получаем валюты, которые можно использовать для оплаты подрядчику и
	// которые настроены для системной конвертации валют.
	systemSettingsTargetCurrencyID := h.Map(filteredSystemSettings, func(item *chainConfiguration) string {
		return item.Target.GetPrimaryKey()
	})

	// Оставляем только те настройки для подрядчиков, которые используют
	// валюты, настроенные для системной конвертации.
	filteredContractorSettings := h.Filter(contractorSettings, func(item EntityWithConversionSettings) bool {
		return h.InArray(item.GetSourceCurrency(), systemSettingsTargetCurrencyID)
	})

	mappedFilteredContractorSettings, err := b.mapSettings(ctx, nil, source, filteredContractorSettings)
	if nil != err {
		return nil, errors.Wrap(err, `failed to map contractor settings`)
	}

	// Остается соединить цепочки системы и подрядчика
	result := make([]conversionChain, 0, len(filteredSystemSettings)*len(mappedFilteredContractorSettings))
	for _, source := range filteredSystemSettings {
		for _, target := range mappedFilteredContractorSettings {
			if nil == source || nil == target {
				continue
			}

			if source.Target.GetPrimaryKey() != target.Source.GetPrimaryKey() {
				continue
			}

			result = append(result, conversionChain{
				SystemConversion:     *source,
				ContractorConversion: *target,
			})
		}
	}

	return result, nil
}

// mapSettings выполняет заполнение цепочки конвертации на основе переданных
// параметров. Если передается source или target, то они подменяют валюты
// из EntityWithConversionSettings.
func (b baseConversionChainGetter[T, C]) mapSettings(
	ctx context.Context,
	source CurrencyEntity,
	target CurrencyEntity,
	items []EntityWithConversionSettings,
) ([]*chainConfiguration, error) {
	result, err := h.MapErr(items, func(item EntityWithConversionSettings) (*chainConfiguration, error) {
		itemTarget := target
		if itemTarget == nil {
			targetCurrency, err := b.currencyIndex.GetByEntityID(ctx, item.GetTargetCurrency())
			if nil != err {
				return nil, errors.Wrap(err, `failed to get target currency`)
			}

			if h.IsEmpty(targetCurrency) {
				return nil, nil
			}

			itemTarget = targetCurrency
		}

		itemSource := source
		if itemSource == nil {
			sourceCurrency, err := b.currencyIndex.GetByEntityID(ctx, item.GetSourceCurrency())
			if nil != err {
				return nil, errors.Wrap(err, `failed to get source currency`)
			}

			if h.IsEmpty(sourceCurrency) {
				return nil, nil
			}

			itemSource = sourceCurrency
		}

		return &chainConfiguration{
			Source:   itemSource,
			Target:   itemTarget,
			Settings: item,
		}, nil
	})

	if nil != err {
		return nil, err
	}

	return h.Filter(result, func(item *chainConfiguration) bool {
		return item != nil
	}), nil
}
