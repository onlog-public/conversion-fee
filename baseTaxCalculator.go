package conversion_fee

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/onlog-public/conversion-fee/rounding"
)

// baseTaxCalculator реализует калькулятор налогов taxCalculator
type baseTaxCalculator[Tax TaxEntity] struct {
	rounding rounding.Service
	taxIndex EntityIndex[Tax]
}

// newTaxCalculator реализует конструктор калькулятора налогов
func newTaxCalculator[Tax TaxEntity](taxIndex EntityIndex[Tax]) taxCalculator {
	return &baseTaxCalculator[Tax]{
		rounding: rounding.New(),
		taxIndex: taxIndex,
	}
}

// Calculate выполняет расчет налога по переданным параметрам
// и возвращает стоимость без налога, с налогом и сам налог.
// Если в метод передан флаг логирования, то будет вестись лог
// расчета налога.
func (b baseTaxCalculator[Tax]) Calculate(
	ctx context.Context,
	price float64,
	currency CurrencyEntity,
	taxID string,
	isTaxIncluded bool,
	isNeedLog bool,
) (priceWithoutTax float64, taxResult float64, log string, err error) {
	tax, err := b.taxIndex.GetByEntityID(ctx, taxID)
	if nil != err {
		return 0, 0, "", errors.Wrap(err, `failed to get tax`)
	}

	priceWithoutTax, taxResult, log = b.calculateTax(
		ctx,
		price,
		currency,
		tax,
		isTaxIncluded,
		isNeedLog,
	)

	return
}

// calculateTax выполняет чистый расчет налога по переданным данным
func (b baseTaxCalculator[Tax]) calculateTax(
	ctx context.Context,
	price float64,
	currency CurrencyEntity,
	taxEntity TaxEntity,
	isTaxIncluded bool,
	isNeedLog bool,
) (float64, float64, string) {
	// Если налог не включен в стоимость, то считаем его по отдельной формуле
	if !isTaxIncluded {
		taxResult := price / float64(100) * float64(taxEntity.GetTaxValue())
		taxLog := ``
		if isNeedLog {
			taxLog = fmt.Sprintf(`%v / 100%% x %v%%`, price, taxEntity.GetTaxValue())
		}

		roundedTaxResult, taxLog := b.rounding.RoundValue(ctx, currency, taxResult, taxLog, isNeedLog)

		if isNeedLog {
			return price, roundedTaxResult, fmt.Sprintf(`Tax (%v :: %v%% [%v = %v] not included)`, price, taxEntity.GetTaxValue(), taxLog, roundedTaxResult)
		}

		return price, roundedTaxResult, ""
	}

	// Налог включен в стоимость и требуется достать из него налог
	taxResult := price * float64(taxEntity.GetTaxValue()) / (100 + float64(taxEntity.GetTaxValue()))
	taxLog := ``
	if isNeedLog {
		taxLog = fmt.Sprintf(`%v * %v%% / (100%% + %v%%)`, price, taxEntity.GetTaxValue(), taxEntity.GetTaxValue())
	}

	roundedTaxResult, taxLog := b.rounding.RoundValue(ctx, currency, taxResult, taxLog, isNeedLog)
	roundedPrice, _ := b.rounding.RoundValue(ctx, currency, price-taxResult, ``, false)

	if isNeedLog {
		return roundedPrice, roundedTaxResult, fmt.Sprintf(`Tax (%v :: %v%% [%v = (price :: %v, tax :: %v)] included)`, price, taxEntity.GetTaxValue(), taxLog, roundedPrice, roundedTaxResult)
	}

	return roundedPrice, roundedTaxResult, ""
}
