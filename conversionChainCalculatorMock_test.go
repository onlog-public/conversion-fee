package conversion_fee

import (
	"context"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// conversionChainWithResult реализует DTO для передачи цепи расчета и результатов
// для тестирования
type conversionChainWithResult struct {
	conversionChain
	ConversionChainCalculationResult
}

// conversionChainCalculatorMock реализует подставку для тестирования
type conversionChainCalculatorMock[T CurrencyEntity, C ContractorEntity] struct {
	Result []conversionChainWithResult
}

// GetConversionChain возвращает наборы подходящих для конвертации цепочек.
// source - валюта ставки подрядчика
// target - целевая валюта пользователя
func (c conversionChainCalculatorMock[T, C]) GetConversionChain(
	_ context.Context,
	_ T,
	_ T,
	_ C,
) ([]conversionChain, error) {
	return h.Map(c.Result, func(item conversionChainWithResult) conversionChain {
		return item.conversionChain
	}), nil
}

// CalculateChain вычисляет стоимость переданной цепочки конверсии.
// Возвращает полностью рассчитанную и переведенную стоимость по этой цепочке.
func (c conversionChainCalculatorMock[T, C]) CalculateChain(
	_ context.Context,
	chain conversionChain,
	_ float64,
	_ bool,
) (ConversionChainCalculationResult, []string) {
	chainRes := h.Find(c.Result, func(item conversionChainWithResult) bool {
		return item.conversionChain.ContractorConversion.Source.GetPrimaryKey() == chain.ContractorConversion.Source.GetPrimaryKey() &&
			item.conversionChain.ContractorConversion.Target.GetPrimaryKey() == chain.ContractorConversion.Target.GetPrimaryKey() &&
			item.conversionChain.SystemConversion.Source.GetPrimaryKey() == chain.SystemConversion.Source.GetPrimaryKey() &&
			item.conversionChain.SystemConversion.Target.GetPrimaryKey() == chain.SystemConversion.Target.GetPrimaryKey()
	})

	if nil == chainRes {
		return ConversionChainCalculationResult{}, nil
	}

	return chainRes.ConversionChainCalculationResult, nil
}
