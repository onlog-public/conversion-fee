module gitlab.com/onlog-public/conversion-fee

go 1.18

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	gitlab.systems-fd.com/packages/golang/helpers/h v0.21.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
