package conversion_fee

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/onlog-public/conversion-fee/rounding"
)

// baseOfferCalculator реализует калькулятор стоимости ценового
// предложения OfferCalculator
type baseOfferCalculator[T CurrencyEntity, C ContractorEntity] struct {
	taxCalculator taxCalculator
	rounding      rounding.Service
	converter     Converter[T, C]
	currencyIndex EntityIndex[T]
	contractorIdx EntityIndex[C]
}

// New реализует конструктор сервиса расчета ценовых предложений.
// Данный сервис реализует функционал расчета не только базовой
// стоимости, но и налогов и конверсионных издержек.
func New[T CurrencyEntity, C ContractorEntity, Tax TaxEntity](
	currencyIndex EntityIndex[T],
	currencySystemSettingsIndex ConversionIndex,
	contractorIndex EntityIndex[C],
	contractorConversionIndex ContractorConversionIndex[C],
	taxIndex EntityIndex[Tax],
) OfferCalculator[T, C] {
	return &baseOfferCalculator[T, C]{
		taxCalculator: newTaxCalculator(taxIndex),
		rounding:      rounding.New(),
		converter:     NewConverter(currencyIndex, currencySystemSettingsIndex, contractorConversionIndex),
		currencyIndex: currencyIndex,
		contractorIdx: contractorIndex,
	}
}

// Calculate выполняет расчет переданного ценового предложения и
// возвращает результат расчета.
// Если передан флаг isNeedLog, то в результатах расчета будет
// возвращаться лог расчета стоимости.
func (b baseOfferCalculator[T, C]) Calculate(
	ctx context.Context,
	offer OfferToCalculate,
	isNeedLog bool,
) (*OfferCalculationResult, []string, error) {
	log := make([]string, 0, 10)

	contractor, err := b.contractorIdx.GetByEntityID(ctx, offer.ContractorID)
	if err != nil {
		return nil, nil, errors.Wrap(err, `failed to get contractor`)
	}

	sourceCurrency, err := b.currencyIndex.GetByEntityID(ctx, offer.SourceCurrency)
	if nil != err {
		return nil, nil, errors.Wrap(err, `failed to get source currency`)
	}

	targetCurrency, err := b.currencyIndex.GetByEntityID(ctx, offer.TargetCurrency)
	if nil != err {
		return nil, nil, errors.Wrap(err, `failed to get target currency`)
	}

	result := &OfferCalculationResult{
		Quantity:  offer.Quantity,
		BasePrice: offer.Price,
	}

	// Если стоимость меньше минимальной, то берем минимальную
	if offer.MinimalPrice > (offer.Quantity * offer.Price) {
		result.Quantity = 1
		result.BasePrice = offer.MinimalPrice
	}

	// Если стоимость фиксирована, то берем фиксу
	if offer.IsFixed {
		result.Quantity = 1
		result.BasePrice = offer.Price
	}

	// Рассчитываем базовую стоимость офера на основе количества и базовой цены
	offerPrice := result.BasePrice * result.Quantity
	offerPriceLog := ``
	if isNeedLog {
		offerPriceLog = fmt.Sprintf("%f * %f", result.BasePrice, result.Quantity)
	}

	roundedFullPrice, offerPriceLog := b.rounding.RoundValue(ctx, sourceCurrency, offerPrice, offerPriceLog, isNeedLog)
	if isNeedLog {
		log = append(log, `Calculated offer base price :: `+offerPriceLog)
	}

	// Рассчитываем базовую стоимость и базовый налог
	basePrice, baseTax, taxLog, err := b.taxCalculator.Calculate(ctx, roundedFullPrice, sourceCurrency, offer.Tax, offer.IsTaxIncluded, isNeedLog)
	if err != nil {
		return nil, nil, errors.Wrap(err, `failed to calculate base tax value`)
	}

	if isNeedLog {
		log = append(log, `Calculated offer base tax :: `+taxLog)
	}

	// Вычисляем стоимость единицы ценового предложения после пересчета налогов
	roundedSinglePrice, _ := b.rounding.RoundValue(ctx, sourceCurrency, basePrice/result.Quantity, ``, false)

	// Считаем полную стоимость ценового предложения в базовой валюте.
	roundedFullPrice, _ = b.rounding.RoundValue(ctx, sourceCurrency, basePrice+baseTax, ``, false)
	result.BasePrice = basePrice
	result.SinglePrice = roundedSinglePrice
	result.BaseTax = baseTax
	result.BaseFullPrice = roundedFullPrice

	// Переводим стоимость в целевую валюту и считаем конверсионные издержки
	conversionResult, conversionLog, err := b.converter.Convert(ctx, sourceCurrency, targetCurrency, contractor, result.BasePrice, offer.DefaultConversion, isNeedLog)
	if err != nil {
		return nil, nil, errors.Wrap(err, `failed to calculate conversion fee and offer price in target currency`)
	}

	if isNeedLog {
		log = append(log, conversionLog...)
	}

	result.InnerConversionFee = conversionResult.ContractorFee
	result.InnerCurrency = conversionResult.InnerCurrency
	result.SystemFee = conversionResult.SystemFee
	result.ConversionFee = conversionResult.SystemFee + conversionResult.ContractorFeeInSystemCurrency

	// Теперь необходимо добавить налог к итоговой стоимости
	price, tax, taxLog, err := b.taxCalculator.Calculate(ctx, conversionResult.ConvertedValue, targetCurrency, offer.Tax, false, isNeedLog)
	if err != nil {
		return nil, nil, errors.Wrap(err, `failed to calculate tax for result value`)
	}

	if isNeedLog {
		log = append(log, `Calculated offer result tax :: `+taxLog)
	}

	result.Price = price
	result.Tax = tax

	fullPrice, _ := b.rounding.RoundValue(ctx, targetCurrency, price+tax, ``, false)
	result.FullPrice = fullPrice

	return result, log, nil
}
