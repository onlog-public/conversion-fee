package conversion_fee

import "gitlab.com/onlog-public/conversion-fee/rounding"

// TestEntity реализует подставку для тестирования округления
type TestEntity struct {
	Configuration rounding.Configuration
}

// GetRoundingConfiguration возвращает параметры конфигурации округления
// значений.
func (t TestEntity) GetRoundingConfiguration() rounding.Configuration {
	return t.Configuration
}
