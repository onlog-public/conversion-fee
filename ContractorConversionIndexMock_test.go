package conversion_fee

// ContractorConversionIndexMock реализует подставку для тестирования
var ContractorConversionIndexMock = func(index ConversionIndex) ContractorConversionIndex[ContractorEntityMock] {
	return func(contractor ContractorEntityMock) (ConversionIndex, error) {
		return index, nil
	}
}
