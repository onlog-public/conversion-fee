package conversion_fee

import (
	"context"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"testing"
)

func Test_baseCurrencyConverter_Convert(t *testing.T) {
	type fields struct {
		roundingService rounding.Service
	}
	type args struct {
		ctx         context.Context
		source      CurrencyEntity
		target      CurrencyEntity
		sourceValue float64
		isNeedLog   bool
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   float64
		want1  string
	}{
		{
			name: "Тестирование конвертации валюты",
			fields: fields{
				roundingService: &roundingServiceMock{},
			},
			args: args{
				ctx: context.Background(),
				source: &CurrencyEntityMock{
					Configuration:                   rounding.Configuration{},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: &CurrencyEntityMock{
					Configuration:                   rounding.Configuration{},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         10,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				sourceValue: 10000,
				isNeedLog:   true,
			},
			want:  50000,
			want1: "10000 × 100 / 1 / 200 × 10",
		},
		{
			name: "Тестирование конвертации валюты",
			fields: fields{
				roundingService: &roundingServiceMock{},
			},
			args: args{
				ctx: context.Background(),
				source: &CurrencyEntityMock{
					Configuration: rounding.Configuration{},
					PrimaryKey:    "1",
					Course:        100,
					Nominal:       1,
				},
				target: &CurrencyEntityMock{
					Configuration: rounding.Configuration{},
					PrimaryKey:    "2",
					Course:        200,
					Nominal:       10,
				},
				sourceValue: 10000,
				isNeedLog:   false,
			},
			want:  50000,
			want1: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseCurrencyConverter{
				roundingService: tt.fields.roundingService,
			}
			got, got1 := b.Convert(tt.args.ctx, tt.args.source, tt.args.target, tt.args.sourceValue, tt.args.isNeedLog)
			if got != tt.want {
				t.Errorf("Convert() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Convert() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
