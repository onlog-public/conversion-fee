package conversion_fee

import (
	"context"
	"gitlab.com/onlog-public/conversion-fee/rounding"
	"reflect"
	"testing"
)

func Test_baseConversionChainGetter_GetConversionChain(t *testing.T) {
	type fields struct {
		currencyIndex       EntityIndex[CurrencyEntityMock]
		systemSettingsIndex ConversionIndex
		contractorIndex     ContractorConversionIndex[ContractorEntityMock]
	}
	type args struct {
		ctx        context.Context
		source     CurrencyEntityMock
		target     CurrencyEntityMock
		contractor ContractorEntityMock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []conversionChain
		wantErr bool
	}{
		{
			name: "Тестирование генерации комбинаций, где все варианты валидны",
			fields: fields{
				currencyIndex: &EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`2`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`3`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`4`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`5`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "5",
							Course:                          500,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
					},
				},
				systemSettingsIndex: &ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "4",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					TargetCurrencySettings: []EntityWithConversionSettings{},
				},
				contractorIndex: ContractorConversionIndexMock(&ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{},
					TargetCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "4",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				}),
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor: ContractorEntityMock{},
			},
			want: []conversionChain{
				{
					SystemConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					ContractorConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				},
				{
					SystemConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "1",
							Target: "4",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					ContractorConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "4",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование генерации комбинаций, где начальная валюта не доступна для выбора",
			fields: fields{
				currencyIndex: &EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      false,
							CurrencyAvailableForContractors: true,
						},
						`2`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`3`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`4`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`5`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "5",
							Course:                          500,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
					},
				},
				systemSettingsIndex: &ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "4",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					TargetCurrencySettings: []EntityWithConversionSettings{},
				},
				contractorIndex: ContractorConversionIndexMock(&ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{},
					TargetCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "4",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				}),
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      false,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor: ContractorEntityMock{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации комбинаций, когда одна из валют не доступна для оплаты подрядчикам",
			fields: fields{
				currencyIndex: &EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`2`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`3`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`4`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: false,
						},
						`5`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "5",
							Course:                          500,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
					},
				},
				systemSettingsIndex: &ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "4",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					TargetCurrencySettings: []EntityWithConversionSettings{},
				},
				contractorIndex: ContractorConversionIndexMock(&ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{},
					TargetCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "4",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				}),
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor: ContractorEntityMock{},
			},
			want: []conversionChain{
				{
					SystemConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					ContractorConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Target: CurrencyEntityMock{
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование генерации комбинаций, когда одна из валют не доступна для оплаты подрядчикам",
			fields: fields{
				currencyIndex: &EntityIndexMock[CurrencyEntityMock]{
					ResultMap: map[string]CurrencyEntityMock{
						`1`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`2`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
						`3`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: false,
						},
						`4`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "4",
							Course:                          400,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: false,
						},
						`5`: {
							Configuration: rounding.Configuration{
								Accuracy: 2,
								Rule:     rounding.ByMath,
							},
							PrimaryKey:                      "5",
							Course:                          500,
							Nominal:                         1,
							CurrencyAvailableForSource:      true,
							CurrencyAvailableForContractors: true,
						},
					},
				},
				systemSettingsIndex: &ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "1",
							Target: "4",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
					TargetCurrencySettings: []EntityWithConversionSettings{},
				},
				contractorIndex: ContractorConversionIndexMock(&ConversionIndexMock{
					SourceCurrencySettings: []EntityWithConversionSettings{},
					TargetCurrencySettings: []EntityWithConversionSettings{
						EntityWithConversionSettingsMock{
							Source: "3",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
						EntityWithConversionSettingsMock{
							Source: "4",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           5,
								FixAmount:         100,
								MinValue:          0,
								MaxValue:          10000,
								IsMinValueLimited: true,
								IsMaxValueLimited: true,
							},
						},
					},
				}),
			},
			args: args{
				ctx: context.Background(),
				source: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "2",
					Course:                          200,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				target: CurrencyEntityMock{
					Configuration: rounding.Configuration{
						Accuracy: 2,
						Rule:     rounding.ByMath,
					},
					PrimaryKey:                      "1",
					Course:                          100,
					Nominal:                         1,
					CurrencyAvailableForSource:      true,
					CurrencyAvailableForContractors: true,
				},
				contractor: ContractorEntityMock{},
			},
			want:    []conversionChain{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseConversionChainGetter[CurrencyEntityMock, ContractorEntityMock]{
				currencyIndex:       tt.fields.currencyIndex,
				systemSettingsIndex: tt.fields.systemSettingsIndex,
				contractorIndex:     tt.fields.contractorIndex,
			}
			got, err := b.GetConversionChain(tt.args.ctx, tt.args.source, tt.args.target, tt.args.contractor)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetConversionChain() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetConversionChain() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_baseConversionChainGetter_CalculateChain(t *testing.T) {
	type fields struct {
		conversionCalculator conversionCalculator
		currencyConverter    currencyConverter
	}
	type args struct {
		ctx       context.Context
		chain     conversionChain
		value     float64
		isNeedLog bool
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   ConversionChainCalculationResult
		want1  []string
	}{
		{
			name: "Тестирование расчета переданной цепочки",
			fields: fields{
				conversionCalculator: &conversionCalculatorMock{},
				currencyConverter: &currencyConverterMock{
					Coefficient: map[string]map[string]float64{
						`2`: {
							`1`: 10,
						},
						`3`: {
							`2`: 20,
						},
					},
				},
			},
			args: args{
				ctx: context.Background(),
				chain: conversionChain{
					SystemConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration:                   rounding.Configuration{},
							PrimaryKey:                      "1",
							Course:                          100,
							Nominal:                         1,
							CurrencyAvailableForSource:      false,
							CurrencyAvailableForContractors: false,
						},
						Target: CurrencyEntityMock{
							Configuration:                   rounding.Configuration{},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      false,
							CurrencyAvailableForContractors: false,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "1",
							Target: "2",
							Configuration: ConversionConfiguration{
								Percent:           10,
								FixAmount:         0,
								MinValue:          0,
								MaxValue:          0,
								IsMinValueLimited: false,
								IsMaxValueLimited: false,
							},
						},
					},
					ContractorConversion: chainConfiguration{
						Source: CurrencyEntityMock{
							Configuration:                   rounding.Configuration{},
							PrimaryKey:                      "2",
							Course:                          200,
							Nominal:                         1,
							CurrencyAvailableForSource:      false,
							CurrencyAvailableForContractors: false,
						},
						Target: CurrencyEntityMock{
							Configuration:                   rounding.Configuration{},
							PrimaryKey:                      "3",
							Course:                          300,
							Nominal:                         1,
							CurrencyAvailableForSource:      false,
							CurrencyAvailableForContractors: false,
						},
						Settings: EntityWithConversionSettingsMock{
							Source: "2",
							Target: "3",
							Configuration: ConversionConfiguration{
								Percent:           20,
								FixAmount:         0,
								MinValue:          0,
								MaxValue:          0,
								IsMinValueLimited: false,
								IsMaxValueLimited: false,
							},
						},
					},
				},
				value:     1000,
				isNeedLog: true,
			},
			want: ConversionChainCalculationResult{
				BaseValue:                     1000,
				ConvertedValue:                1320,
				ContractorFee:                 200,
				ContractorFeeInSystemCurrency: 2000,
				SystemFee:                     120,
				InnerCurrency:                 "2",
			},
			want1: []string{
				`Calculating contractors conversion fee: 200`,
				`Calculation contractor total payment: 1200`,
				`Calculating system user conversion fee: 120`,
				`Calculation system user total payment: 1320`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseConversionChainGetter[CurrencyEntityMock, ContractorEntityMock]{
				conversionCalculator: tt.fields.conversionCalculator,
				currencyConverter:    tt.fields.currencyConverter,
			}
			got, got1 := b.CalculateChain(tt.args.ctx, tt.args.chain, tt.args.value, tt.args.isNeedLog)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CalculateChain() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CalculateChain() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
