package conversion_fee

// ContractorEntity описывает интерфейс сущности подрядчика
type ContractorEntity interface {
	// GetPrimaryKey возвращает первичный ключ подрядчика
	GetPrimaryKey() string
}
